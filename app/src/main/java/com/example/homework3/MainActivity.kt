package com.example.homework3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter

    private var titleN = "title"
    private var descriptionN = "description"
    private var dateN = "date"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        addItemButton.setOnClickListener {
            openAddItem()
            adapter.notifyItemInserted(0)
            recyclerview.scrollToPosition(0)
        }

        addInfo()

        setData()

        recyclerview.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items, this)
        recyclerview.adapter = adapter
    }

    private fun setData() {
        items.add(ItemModel(R.mipmap.bbyd, "title1", "description1", "2020-01-01 00:00"))
        items.add(ItemModel(R.mipmap.bbyd, "title2", "description2", "2018-06-25 20:21"))
        items.add(ItemModel(R.mipmap.bbyd, "title3", "description3", "2017-03-08 08:32"))
        items.add(ItemModel(R.mipmap.bbyd, "title4", "description4", "2020-09-01 17:01"))
        items.add(ItemModel(R.mipmap.bbyd, "title5", "description5", "2019-02-10 12:24"))
        items.add(ItemModel(R.mipmap.bbyd, "title6", "description6", "2020-01-15 15:20"))
    }

    private fun openAddItem() {
        val intent = Intent (this, AddItem::class.java)
        startActivity(intent)
    }

    private fun addInfo() {
        val intent = intent.extras

        if (intent != null) {
            titleN = intent!!.getString("title", "title")
            descriptionN = intent.getString("description", "description")
            dateN = intent.getString("date", "date")
            items.add(0, ItemModel(R.mipmap.bbyd, titleN, descriptionN, dateN))
        }
    }

}
